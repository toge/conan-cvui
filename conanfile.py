import os

from conans import ConanFile, tools

class CvuiConan(ConanFile):
    name = "cvui"
    version = "2.7.0"
    license = "MIT"
    author = "toge.mail@gmail.com"
    url = "https://bitbucket.org/toge/conan-cvui/"
    homepage = "https://github.com/Dovyski/cvui"
    description = "A (very) simple UI lib built on top of OpenCV drawing primitives."
    topics = ("opencv", "UI", "GUI", "opencv-drawing-primitives")
    no_copy_source = True
    requires = ("opencv/[>= 4.0.0]@conan/stable")
    # No settings/options are necessary, this is header only

    def source(self):
        '''retrieval of the source code here. Remember you can also put the code
        in the folder and use exports instead of retrieving it with this
        source() method
        '''
        tools.get("https://github.com/Dovyski/cvui/archive/v{}.zip".format(self.version))
        os.rename("cvui-{}".format(self.version), "cvui")

    def package(self):
        self.copy("cvui.h", "include", src="cvui")
